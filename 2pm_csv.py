import pywhatkit as kit
import datetime
import csv

# Ruta al archivo CSV
csv_file_path = "bd-afiliados-.csv"

# Lista para almacenar los números de teléfono
phone_numbers = []

# Leer el archivo CSV y obtener los números de la cuarta columna
with open(csv_file_path, newline='') as csvfile:
    csv_reader = csv.reader(csvfile)
    for row in csv_reader:
        # Asegurarse de que la fila tiene al menos 4 columnas
        if len(row) >= 4:
            phone_numbers.append(row[3])

# Mensaje que quieres enviar
message = """
Hola, ¿aún no has ido a votar? ¡Todavía estás a tiempo de que tu voto cuente!

Sé parte de la solución y haz que se escuche tu voz a través del voto. Si necesitas ayuda para encontrar tu casilla con gusto podemos apoyarte. Ubica el número de sección que viene en tu INE. Puedes acceder en la siguiente liga:https://ubicatucasilla.ine.mx o mandarnos un mensaje y te ayudamos. 

Conoce las promociones por ser un ciudadano responsable y haber votado 👍:  https://ieebc.mx/red-aliados-promocion-voto-2024/

Si ya votaste gracias por ser parte de la solución 
Mexicali agradece tu voto 🗳️"""

#timestamp par medir cuanto tiempo toma enviar el mensaje 
# Obtener los milisegundos actuales
timestamp = datetime.datetime.now().timestamp() * 1000
# Enviar el mensaje a cada numero en la lista
for phone_number in phone_numbers:
    try:
        # Validar que el número de teléfono solo contenga dígitos
        if not phone_number.isdigit():
            continue

        phone = ""
        if phone_number.startswith("1760"):
            phone = "+" + phone_number
        else:
            phone = "+52" + phone_number

        print("Enviando mensaje a: ", phone)

        # Usar sendwhatmsg_instantly para enviar el mensaje inmediatamente
        kit.sendwhatmsg_instantly(phone, message, 15, True, 2)
        # print para confirmar que se envio el mensaje y el tiempo que paso 
        # Calcular el tiempo transcurrido desde el ultimo timestamp
        tiempo_transcurrido = datetime.datetime.now().timestamp() * 1000 - timestamp
        print(f"Mensaje enviado a {phone}. Tiempo transcurrido: {tiempo_transcurrido:.2f} milisegundos")
    except Exception as e:
        print(f"Error al enviar mensaje a {phone}. Error: {str(e)}")

# tiempo total que tomo enviar todos los mensajes
# Calcular el tiempo total transcurrido desde que se definió el timestamp
tiempo_total_transcurrido = datetime.datetime.now().timestamp() * 1000 - timestamp
print(f"Tiempo total transcurrido: {tiempo_total_transcurrido:.2f} milisegundos")

print("Done!")