import csv

# Archivo original
input_file = 'bd-afiliados.csv'

# Nombres de los archivos de salida
output_files = [
    'bd-afiliados-1.csv',
    'bd-afiliados-2.csv',
    'bd-afiliados-3.csv'
]

# Leer todas las lineas del archivo CSV original
with open(input_file, 'r') as csvfile:
    reader = csv.reader(csvfile)
    lines = list(reader)

# Obtener la primera linea (encabezados)
header = lines[0]

# Numero de lineas de datos (excluyendo el encabezado)
num_lines = len(lines) - 1

# Calcular el tamano de cada archivo
lines_per_file = num_lines // 3

# Crear y escribir en los archivos de salida
for i in range(3):
    start_index = i * lines_per_file + 1
    if i == 2:
        end_index = num_lines + 1
    else:
        end_index = (i + 1) * lines_per_file + 1
    
    with open(output_files[i], 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(header)
        writer.writerows(lines[start_index:end_index])

print("Archivos divididos exitosamente.")
